import pygame
import socket
import random
 
# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
 
 
class Block(pygame.sprite.Sprite):
    """
    This class represents the ball
    It derives from the "Sprite" class in Pygame
    """
 
    def __init__(self, color, width, height):
        """ Constructor. Pass in the color of the block,
        and its x and y position. """
        # Call the parent class (Sprite) constructor
        super().__init__()
 
        # Create an image of the block, and fill it with a color.
        # This could also be an image loaded from the disk.
        self.image = pygame.Surface([width, height])
        self.image.fill(color)
 
        # Fetch the rectangle object that has the dimensions of the image
        # image.
        # Update the position of this object by setting the values
        # of rect.x and rect.y
        self.rect = self.image.get_rect()
 
        # Instance variables that control the edges of where we bounce
        self.left_boundary = 0
        self.right_boundary = 0
        self.top_boundary = 0
        self.bottom_boundary = 0
 
        # Instance variables for our current speed and direction
        self.change_x = 0
        self.change_y = 0
 
    def update(self):
        """ Called each frame. """
        self.rect.x += self.change_x
        self.rect.y += self.change_y
 
        if self.rect.left <= self.left_boundary: 
            self.change_x *= -1
 
        if self.rect.bottom >= self.bottom_boundary or self.rect.top <= self.top_boundary:
            self.change_y *= -1

        if self.rect.right >= self.right_boundary:
            if left_ip == None or right_ip == None:
                self.change_x *= -1
                
            else:
                # Left Screen
                try:
                    my_message = "{},{},{}\n".format(self.rect.y, self.change_x, self.change_y)
                    send_message = bytearray()
                    send_message.extend(map(ord, my_message))

                    self.kill()
                    #print("Message sent: ", send_message)
    
                    send_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    send_socket.connect((right_ip, right_port))
                    send_socket.sendall(send_message)
                    send_socket.close()
                
                except:
                    print("Unable to send the message.")
                    self.change_x = self.change_x * -1                
 
 

# Initialize Pygame
pygame.init()
 
# Set the height and width of the screen
screen_width = 700
screen_height = 400
screen = pygame.display.set_mode([screen_width, screen_height])
 
 
# This is a list of every sprite. All blocks and the player block as well.
all_sprites_list = pygame.sprite.Group()
 
# This represents a block
block = Block(RED, 20, 15)
 
# Set a random location for the block
block.rect.x = random.randrange(screen_width)
block.rect.y = random.randrange(screen_height)
 
block.change_x = 3
block.change_y = 3 
block.left_boundary = 0
block.top_boundary = 0
block.right_boundary = screen_width - 3
block.bottom_boundary = screen_height - 3
 
# Add the block to the list of objects
all_sprites_list.add(block)
 
 
# Loop until the user clicks the close button.
done = False
 
# Used to manage how fast the screen updates
clock = pygame.time.Clock()
 
# TCP Setup
left_ip = '192.168.1.114'
left_port = 5006

right_ip = '192.168.1.124' 
right_port = 5005

BUFFER_SIZE = 65535
NO_CONNECTION = 1
CONNECTED = 2
        
state = NO_CONNECTION
 
# -------- Main Program Loop -----------
while not done:
    if left_ip == None or right_ip == None:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
             
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    block = Block(BLACK, 20, 15)
                    block.rect.x = random.randrange(screen_width)
                    block.rect.y = random.randrange(screen_height)
                    block.change_x = 3
                    block.change_y = 3
                    block.left_boundary = 0
                    block.top_boundary = 0
                    block.right_boundary = screen_width - 3
                    block.bottom_boundary = screen_height - 3
    
                    all_sprites_list.add(block)
     
        # Clear the screen
        screen.fill(WHITE)
     
        # Calls update() method on every sprite in the list
        all_sprites_list.update()
     
     
     
        # Draw all the spites
        all_sprites_list.draw(screen)
     
        # Limit to 60 frames per second
        clock.tick(60)
     
        # Go ahead and update the screen with what we've drawn.
        pygame.display.flip()
        
    else:
        # TCP Listener
        listening_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        listening_socket.settimeout(0.0)
        listening_socket.bind((left_ip, left_port))
        listening_socket.listen(1)   
        
        while True: 
            if state == NO_CONNECTION:
                try:
                    connection, client_address = listening_socket.accept()
                    state = CONNECTED

                except BlockingIOError:
                    pass
            
            if state == CONNECTED:
                try:
                    data = connection.recv(BUFFER_SIZE)
                    if len(data) > 0:
                        #print("Data Received: ", data)
                        string_data = data.decode("utf-8")
                        decoded_data = string_data.strip().split(",")
                        #print("Data:", decoded_data)

                        # Create Block
                        block = Block(BLACK, 20, 15)
                        block.rect.x = screen_width - 21
                        block.rect.y = int(decoded_data[0])
                        block.change_x = int(decoded_data[1])
                        block.change_y = int(decoded_data[2])
                        block.left_boundary = 0
                        block.top_boundary = 0
                        block.right_boundary = screen_width
                        block.bottom_boundary = screen_height

                        all_sprites_list.add(block)
 
                        state = NO_CONNECTION
                        connection.close()
            
                except BlockingIOError:
                    pass  
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True
             
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        block = Block(BLACK, 20, 15)
                        block.rect.x = random.randrange(screen_width)
                        block.rect.y = random.randrange(screen_height)
                        block.change_x = 3
                        block.change_y = 3
                        block.left_boundary = 0
                        block.top_boundary = 0
                        block.right_boundary = screen_width
                        block.bottom_boundary = screen_height
    
                        all_sprites_list.add(block)
     
            # Clear the screen
            screen.fill(WHITE)
     
            # Calls update() method on every sprite in the list
            all_sprites_list.update()
     
     
     
            # Draw all the spites
            all_sprites_list.draw(screen)
     
            # Limit to 60 frames per second
            clock.tick(60)
     
            # Go ahead and update the screen with what we've drawn.
            pygame.display.flip()        
 
pygame.quit()
